<?php
/**
 * Plugin Name: PODS Layout Theme Options
 * Description: Add theme options tab using ACF
 */

namespace PODS\LayoutThemeOptions;

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}


if (!class_exists('LayoutThemeOptions')) {
    class LayoutThemeOptions
    {

        // vars
        var $settings;


        /*
        *  __construct
        *
        *  A dummy constructor to ensure initialized once
        */

        function __construct() {

            /* Do nothing here */

        }


        /*
        *  initialize
        *
        *  The real constructor to initialize
        *
        */

        public function initialize()
        {

            // vars
            $this->settings = array(
                // basic
                'name'              => __('PODS LayoutThemeOptions'),
                'version'           => '0.1',

                // urls
                'basename'          => plugin_basename( __FILE__ ),
                'path'              => plugin_dir_path( __FILE__ ),
                'dir'               => plugin_dir_url( __FILE__ ),
            );

            // actions
            add_action( 'admin_menu', array( $this, 'acf_options_menu' ), 12 );

        }

        /**
         * Advanced Custom Fields customizations
        */
        function acf_options_menu()
        {
          if ( function_exists('acf_add_options_page') && function_exists('acf_add_options_sub_page') )
          {
            $options = acf_add_options_page(
              array(
                'page_title'  => 'Theme Options',
                'menu_title'  => 'Theme Options',
                'menu_slug'   => 'theme-options',
                'capability'  => 'edit_posts',
                'redirect'    => true
              )
            );

            $options_settings = acf_add_options_sub_page(
              array(
                'title' => 'Settings',
                'parent' => 'theme-options',
                'capability' => 'manage_options'
              )
            );

            $options_homepage = acf_add_options_sub_page(
              array(
                'title' => 'Homepage',
                'parent' => 'theme-options',
                'capability' => 'manage_options'
              )
            );
          }
        }
    }
}


/**
 * Instantiates the LayoutThemeOptions
 */
function runLayoutThemeOptions()
{
    $run = new LayoutThemeOptions();
    $run->initialize();
}

runLayoutThemeOptions();
